import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as BiIcons from "react-icons/bi";
import * as SiIcons from "react-icons/si";

export const DATA = [
  {
    title: "Home",
    path: "/",
    icon: <AiIcons.AiFillHome />,
    cName: "sidebar-text",
  },
  {
    title: "Manage Users",
    path: "/manage_users",
    icon: <FaIcons.FaUser />,
    cName: "sidebar-text",
  },
  {
    title: "Manage Content",
    path: "/manage_contacts",
    icon: <BiIcons.BiBookContent />,
    cName: "sidebar-text",
  },
  {
    title: "Analytics Dashboard",
    path: "/analytics_dashboard",
    icon: <SiIcons.SiSimpleanalytics />,
    cName: "sidebar-text",
  },
  {
    title: "Onboarding",
    path: "/onboarding",
    icon: <AiIcons.AiOutlineFundProjectionScreen />,
    cName: "sidebar-text",
  },
];
