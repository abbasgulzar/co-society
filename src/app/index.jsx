import React from "react";
import Navbar from "../components/Navbar";
import LeftSidebar from "../components/LeftSidebar";
import RightSidebar from "../components/RightSidebar";
import Home from "../components/subComponents/Home";
import styled from "styled-components";

const App = () => {
  return (
    <Container>
      <Navbar />
      <div style={{ display: "flex", flexDirection: "row" }}>
        <LeftSidebar />
        <Home />
        <RightSidebar />
      </div>
    </Container>
  );
};

export default App;

const Container = styled.div`
  background-color: whitesmoke;
  height: 100vh;
`;
