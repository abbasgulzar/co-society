import React from "react";
import "./style.css";
import { DATA } from "../../constant/LeftSidebarData";

function LeftSidebar() {
  return (
    <>
      <ul className="left-sidebar">
        {DATA.map((item, index) => {
          return (
            <li key={index} className={item.cName}>
              {item.icon}
              <span>{item.title}</span>
            </li>
          );
        })}
      </ul>
    </>
  );
}

export default LeftSidebar;
