import React from "react";
import MenuIcon from "@mui/icons-material/Menu";
import { Search } from "@mui/icons-material";
import { Avatar } from "@mui/material";
import "./style.css";

const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-alignment ">
        <div className="navbar-item">
          <MenuIcon style={{ color: "white" }} />
          <h1 className="logo">Co.Society</h1>

          <div style={{ position: "relative" }}>
            <input
              className="search-input"
              type="text"
              placeholder="Search users,rooms or settings"
            />
            <div
              style={{
                position: "absolute",
                right: 145,
                top: 2,
                color: "black",
              }}
            >
              <Search />
            </div>
          </div>
        </div>

        <div className="navbar-item">
          <div className="user-detail">
            <div className="details">
              <span className="">Mohsin Javed</span>
              <span className="">Online</span>
            </div>
            <div className="avatar-border">
              <Avatar
                alt="Remy Sharp"
                name="Mohsin Javed"
                sx={{ width: 56, height: 56 }}
              />
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
