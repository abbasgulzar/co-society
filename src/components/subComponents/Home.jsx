import {
  Bed,
  Group,
  GroupAdd,
  Loop,
  Notifications,
  Person2,
  PersonAdd,
  RoomService,
} from "@mui/icons-material";
import React from "react";
import styled from "styled-components";
import CustomCard from "./CustomCard";
import { Card } from "@mui/material";

function Home() {
  return (
    <div>
      <Content>
        <CustomCard
          button="Manage"
          icon={<Person2 style={{ marginRight: 10 }} />}
          title="Users"
        >
          <div>Active</div>
          <div style={{ fontSize: "25px", fontWeight: "500" }}>31</div>

          <a href="#" style={{ fontSize: 12, marginTop: 30 }}>
            Add User
          </a>
        </CustomCard>

        <CustomCard
          icon={<Notifications style={{ marginRight: 10 }} />}
          title="Alerts"
          button="View All"
        ></CustomCard>

        <CustomCard
          icon={<Bed style={{ marginRight: 10 }} />}
          title="Rooms"
        ></CustomCard>

        <CustomCard
          icon={<Group style={{ marginRight: 10 }} />}
          title="Onboarding Users"
          button="Details"
        >
          <div>Active</div>
          <div style={{ fontSize: "25px", fontWeight: "500" }}>3</div>

          <a href="#" style={{ fontSize: 12, marginTop: 30 }}>
            Invite to Onboarding
          </a>
        </CustomCard>

        <CustomCard
          icon={<Loop style={{ marginRight: 10 }} />}
          title="News"
        ></CustomCard>

        <div>
          <MyCard>
            <PersonAdd style={{ width: 35, height: 35 }} />
            <div style={{ fontWeight: "600", fontSize: 16, marginLeft: 20 }}>
              Invite people to Organisation
            </div>
          </MyCard>
          <MyCard>
            <GroupAdd style={{ width: 35, height: 35 }} />
            <div style={{ fontWeight: "600", fontSize: 16, marginLeft: 20 }}>
              Invite people to Onboarding
            </div>
          </MyCard>
        </div>
      </Content>
    </div>
  );
}

export default Home;

const Content = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex-wrap: wrap;
  padding: 25px;
  margin-top: 50px;
  padding-left: 300px;
  padding-right: 200px;
`;

const MyCard = styled(Card)`
  width: 350px;
  height: 80px;
  margin: 8px;
  border-radius: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 20px;
`;
