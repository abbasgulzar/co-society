import React from "react";
import * as MdIcons from "react-icons/md";
import "./style.css";

const RightSidebar = () => {
  return (
    <>
      <div className="right-sidebar">
        <h1 style={{ fontWeight: "bold" }}>Help</h1>
        <div className="get-in-touch">
          <div>
            <MdIcons.MdTouchApp size="40" />
          </div>

          <div className="get-in-touch-paragraph">
            <h3 style={{ fontWeight: "bold" }}>Get in Touch</h3>
            <p style={{ fontWeight: "lighter", fontSize: "1vh" }}>
              Get in touch with our support team to help you reolve the problem
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default RightSidebar;
